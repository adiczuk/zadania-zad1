# -*- coding: utf-8 -*-

import socket
import logging
import logging.config

def server(logger):
    server_address = ('194.29.175.240', 19991) # 194.29.175.240

    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    s.bind(server_address)

    logger.info(u'Tworze server...')
    s.listen(1)

    try:
        while True:
            logger.info(u'Czekam na polaczenie...')
            conn, addr = s.accept()

            logger.info(u'Polaczono z {0}:{1}'.format(*addr))

            try:
                liczby = str.split(conn.recv(1024))

                wynik = float(liczby[0]) + float(liczby[1])
                logger.info(u'Sumuje liczby {0} i {1} oraz odsylam wynik = {2}...'.format(liczby[0], liczby[1], wynik))
                conn.send('{0}'.format(wynik))

            finally:
                logger.info(u'Zamykam polaczenie...')
                conn.close()
    except KeyboardInterrupt:
        s.close()

if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('sum_server')
    server(logger)