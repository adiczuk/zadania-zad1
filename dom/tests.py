# -*- coding: utf-8 -*-

import socket
import unittest

from sum_client import client

class Logger:

    def __init__(self):
        self.data = []

    def info(self, komunikat):
        self.data.append(komunikat)

class TestSumowania(unittest.TestCase):

    wiadomosc = u'Wysylam liczby do sumowania: {0} i {1}...Wynik: {0} + {1} = {2}'

    def setUp(self):
        self.logger = Logger()

    def test1(self):
        self.sumowanie(4, 6)

    def test2(self):
        self.sumowanie(-41, 5)

    def test3(self):
        self.sumowanie(0, 3)

    def test4(self):
        self.sumowanie('a', 7)

    def test5(self):
        self.sumowanie("abc", 3)

    def test6(self):
        self.sumowanie("-6", 3)

    def sumowanie(self, x, y):
        self.sumuj(x, y)
        try:
            x = float(x)
            y = float(y)
            oczekiwana_wiadomosc = self.wiadomosc.format(x, y, x + y)
        except:
            oczekiwana_wiadomosc = 'Brak danych w loggerze'

        otrzymana_wiadomosc = self.process_log()

        self.assertEqual(oczekiwana_wiadomosc, otrzymana_wiadomosc)

    def process_log(self):
        if not self.logger.data:
            return 'Brak danych w loggerze'
        return self.logger.data[1] + self.logger.data[2]

    def sumuj(self, x, y):
        try:
            client(x, y, self.logger)
        except socket.error as e:
            if e.errno == 61:
                msg = u'Blad: {0}, czy serwer dziala?'
                self.fail(msg.format(e.strerror))
            else:
                self.fail(u'Nieznany blad: {0}'.format(str(e)))


if __name__ == '__main__':
    unittest.main()