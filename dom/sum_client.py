# -*- coding: utf-8 -*-

import socket
import logging
import logging.config

def client(x, y, logger):

    if not (isinstance(x, float) and isinstance(y, float)):
        try:
            x = float(x)
            y = float(y)
        except ValueError:
            print('Zle dane!')
            return 0

    server_address = ('194.29.175.240', 19991) # 194.29.175.240

    s = socket.socket()

    logger.info(u'Zaczynam laczyc...')
    s.connect(server_address)

    try:
        logger.info(u'Wysylam liczby do sumowania: {0} i {1}...'.format(x, y))
        s.sendall('{0} {1}'.format(x,y))

        wynik = s.recv(1024)
        logger.info(u'Wynik: {0} + {1} = {2}'.format(x, y, wynik))

    finally:
        s.close()
        logger.info(u'Rozlaczam...')


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('sum_client')
    l1 = raw_input('Liczba pierwsza: ')
    l2 = raw_input('Liczba druga: ')
    client(l1, l2, logger)
    input("Nacisnij klawisz, aby kontynuowac...")
